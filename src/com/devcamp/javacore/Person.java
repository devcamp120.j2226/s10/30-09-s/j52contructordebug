package com.devcamp.javacore;

public class Person{
    //thuộc tính
    public String name;//ten
    public int age;//tuoi
    double weight;//can nang
    long salary;//thu nhap
    String[] pets;//thu nuoi trong nha

    //ham khoi tao khong co tham so
    public Person() {
        this.name = "Tran Van Nam";
        this.age = 20;
        this.weight = 50;
        this.salary = 20000000;
        this.pets = new String[] {"Cat", "Dog"};
    }
    //hàm khởi tạo 2 tham số
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //ham khoi tao co 5 tham so
    public Person(String name, int age, double weight, long salary, String[] pets) {
        //this(name, age);
        // this.name = name;
        // this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
        //this.Infor();
    }

    //phương thức thường không có trả về
    public void ShowInfo() {
        System.out.println("Ten: " + this.name);
        System.out.println("Tuoi: " + this.age);
        System.out.println("Can nang: " + this.weight);
        System.out.println("Thu nhap: " + this.salary);
        System.out.println("Vat nuoi: ");
        if (this.pets != null) {
            for (String pet : this.pets) {
                System.out.print(pet + ",");
            }    
        }
    }

    //phương thức thường có trả về kết quả
    public String Infor() {
        return this.name + "," + this.age;
    }
    
    @Override
    public String toString() {
        String result = "Ten: " + this.name;
        result += ",Tuoi: " + this.age;
        result += ",Can nang: " + this.weight;
        result += ",Thu nhap: " + this.salary;
        result += ",Vat nuoi: [";
        if (this.pets != null) {
            for (String pet : this.pets) {
                result += pet + ",";
            }    
        }
        result += "]";

        return result;
    }    
}
