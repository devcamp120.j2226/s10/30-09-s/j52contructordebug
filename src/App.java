import java.util.ArrayList;

import com.devcamp.javacore.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Person person1 = new Person();
        //person1.ShowInfo();
        // person1.name = "Nguyen Van Nam";
        // person1.age = 18;
        // System.out.println(person1.name);
        // System.out.println(person1.age);

        Person person2 = new Person("Nguyen Van Nam", 18);
        //person2.ShowInfo();
        // person2.name = "Nguyen Van Nam";
        // person2.age = 18;
        // System.out.println(person2.name);
        // System.out.println(person2.age);        
        System.out.println(person1);

        ArrayList<Person> listPerson = new ArrayList<>();
        listPerson.add(person1);
        //listPerson.add(person2);
        listPerson.add(new Person());
        listPerson.add(new Person("Hoang Van Thang", 30, 60, 40000000, new String[] {"Bird", "Cat", "Dog"}));

        for (int i = 0; i < listPerson.size(); i++) {
            Person person = listPerson.get(i);
            person.ShowInfo();
        }
    }
}
